package entity;

import service.EntityService;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;

public class Masterpiece implements EntityService {
  private String name;

  private String authorName;

  private Short createdAt;

  private String type;

  private Scanner in = new Scanner(System.in);

  public Masterpiece() {
  }

  public Masterpiece(String name, String authorName, Short createdAt, String type) {
    this.name = name;
    this.authorName = authorName;
    this.createdAt = createdAt;
    this.type = type;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getAuthorName() {
    return authorName;
  }

  public void setAuthorName(String authorName) {
    this.authorName = authorName;
  }

  public Short getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Short createdAt) {
    this.createdAt = createdAt;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  @Override
  public String toString() {
    return "Masterpiece{" +
        "name='" + name + '\'' +
        ", authorName='" + authorName + '\'' +
        ", createdAt=" + createdAt +
        ", type=" + type +
        '}';
  }

  @Override
  public void input() {
    System.out.println("Input authorName:");
    this.authorName = in.nextLine();

    System.out.println("Input createdAt:");
    this.createdAt = in.nextShort();

    System.out.println("Input name:");
    this.name = in.nextLine();

    System.out.println("Input type:");
    this.type = in.nextLine();
  }

  @Override
  public void method() {
    LocalDateTime localDateTime = LocalDateTime.now();
    System.out.println(localDateTime.getYear() - createdAt);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o)
      return true;
    if (o == null || getClass() != o.getClass())
      return false;

    Masterpiece that = (Masterpiece) o;

    if (!name.equals(that.name))
      return false;
    if (!Objects.equals(authorName, that.authorName))
      return false;
    return Objects.equals(createdAt, that.createdAt);
  }

  @Override
  public int hashCode() {
    int result = name.hashCode();
    result = 31 * result + (authorName != null ? authorName.hashCode() : 0);
    result = 31 * result + (createdAt != null ? createdAt.hashCode() : 0);
    // https://habr.com/ru/post/168195/
    // it can be return createdAt * createdAt
    return result;
  }
}
