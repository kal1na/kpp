package entity;

import service.EntityService;

import java.util.Objects;
import java.util.Scanner;

public class Brigade implements EntityService {
  private String date;

  private Byte employeeCount;

  private Integer plan;

  private Integer fact;

  private Scanner in = new Scanner(System.in);

  public Brigade() {
  }

  public Brigade(String date, Byte employeeCount, Integer plan, Integer fact) {
    this.date = date;
    this.employeeCount = employeeCount;
    this.plan = plan;
    this.fact = fact;
  }

  public String getDate() {
    return date;
  }

  public void setDate(String date) {
    this.date = date;
  }

  public Byte getEmployeeCount() {
    return employeeCount;
  }

  public void setEmployeeCount(Byte employeeCount) {
    this.employeeCount = employeeCount;
  }

  public Integer getPlan() {
    return plan;
  }

  public void setPlan(Integer plan) {
    this.plan = plan;
  }

  public Integer getFact() {
    return fact;
  }

  public void setFact(Integer fact) {
    this.fact = fact;
  }

  @Override
  public String toString() {
    return "Brigade{" +
        "date='" + date + '\'' +
        ", employeeCount=" + employeeCount +
        ", plan=" + plan +
        ", fact=" + fact +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o)
      return true;
    if (o == null || getClass() != o.getClass())
      return false;

    Brigade brigade = (Brigade) o;

    if (!Objects.equals(date, brigade.date))
      return false;
    if (!employeeCount.equals(brigade.employeeCount))
      return false;
    if (!Objects.equals(plan, brigade.plan))
      return false;
    return Objects.equals(fact, brigade.fact);
  }

  @Override
  public int hashCode() {
    int result = date != null ? date.hashCode() : 0;
    result = 31 * result + employeeCount.hashCode();
    result = 31 * result + (plan != null ? plan.hashCode() : 0);
    result = 31 * result + (fact != null ? fact.hashCode() : 0);
    // https://habr.com/ru/post/168195/
    // it can be return employeeCount * employeeCount
    return result;
  }

  @Override
  public void input() {
    System.out.println("Input date:");
    this.date = in.nextLine();

    System.out.println("Input employeeCount:");
    this.employeeCount = in.nextByte();

    System.out.println("Input plan:");
    this.plan = in.nextInt();

    System.out.println("Input fact:");
    this.fact = in.nextInt();
  }

  @Override
  public void method() {
    System.out.println(fact / plan * 100 + "%");
  }
}
