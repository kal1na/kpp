package entity;

import service.EntityService;

import java.util.Objects;
import java.util.Scanner;

public class Film implements EntityService {
  private String name;

  private String authorName;

  private Short createdAt;

  private Integer cost;

  private Integer finalProfit;

  private Scanner in = new Scanner(System.in);

  public Film() {
  }

  public Film(String name, String authorName, Short createdAt, Integer cost, Integer finalProfit) {
    this.name = name;
    this.authorName = authorName;
    this.createdAt = createdAt;
    this.cost = cost;
    this.finalProfit = finalProfit;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getAuthorName() {
    return authorName;
  }

  public void setAuthorName(String authorName) {
    this.authorName = authorName;
  }

  public Short getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Short createdAt) {
    this.createdAt = createdAt;
  }

  public Integer getCost() {
    return cost;
  }

  public void setCost(Integer cost) {
    this.cost = cost;
  }

  public Integer getFinalProfit() {
    return finalProfit;
  }

  public void setFinalProfit(Integer finalProfit) {
    this.finalProfit = finalProfit;
  }

  @Override
  public String toString() {
    return "Film{" +
        "name='" + name + '\'' +
        ", authorName='" + authorName + '\'' +
        ", createdAt=" + createdAt +
        ", cost=" + cost +
        ", finalProfit=" + finalProfit +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o)
      return true;
    if (o == null || getClass() != o.getClass())
      return false;

    Film film = (Film) o;

    if (!name.equals(film.name))
      return false;
    if (!authorName.equals(film.authorName))
      return false;
    if (!createdAt.equals(film.createdAt))
      return false;
    if (!cost.equals(film.cost))
      return false;
    return Objects.equals(finalProfit, film.finalProfit);
  }

  @Override
  public int hashCode() {
    int result = name.hashCode();
    result = 31 * result + authorName.hashCode();
    result = 31 * result + createdAt.hashCode();
    result = 31 * result + cost.hashCode();
    result = 31 * result + (finalProfit != null ? finalProfit.hashCode() : 0);
    // https://habr.com/ru/post/168195/
    // it can be return cost * cost
    return result;
  }

  @Override
  public void input() {
    System.out.println("Input authorName:");
    this.authorName = in.nextLine();

    System.out.println("Input cost:");
    this.cost = in.nextInt();

    System.out.println("Input createdAt:");
    this.createdAt = in.nextShort();

    System.out.println("Input finalProfit:");
    this.finalProfit = in.nextInt();

  }

  @Override
  public void method() {
    System.out.println(finalProfit - cost);
  }
}
