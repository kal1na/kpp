package service;

public interface EntityService {
  void input();

  void method();

  default void output() {
    System.out.println(this);
  }
}
