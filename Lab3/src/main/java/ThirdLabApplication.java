import entity.Brigade;
import entity.Film;
import entity.Masterpiece;
import service.EntityService;

import java.util.ArrayDeque;
import java.util.LinkedList;

public class ThirdLabApplication {

  public static void main(String[] args) {
    LinkedList<EntityService> linkedList = new LinkedList<>();

    ArrayDeque<Object> arrayDeque = new ArrayDeque<>(6);
    Brigade brigade = new Brigade("25/10/2001", (byte)5, 3,3);
    Film film = new Film("Name1", "authorName1",(short)2001, 200, 3000);
    Masterpiece masterpiece = new Masterpiece("Name2", "authorName2",(short)2001, "Film");

    linkedList.add(brigade);
    linkedList.push(film);
    linkedList.push(masterpiece);
    linkedList.forEach(element -> {
      element.output();
      element.method();
    });

    Long longNumber = 8L;
    Short shortNumber = film.getCreatedAt();
    Character character = film.getAuthorName().charAt(0);

    arrayDeque.add(brigade);
    arrayDeque.push(film);
    arrayDeque.push(masterpiece);
    arrayDeque.add(longNumber);
    arrayDeque.add(shortNumber);
    arrayDeque.add(character);

    arrayDeque.forEach(element -> {
      System.out.println(element.toString());
      System.out.println(element.toString());
    });
  }

}
