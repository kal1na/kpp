package ua.od.atomspace;

import org.junit.Test;
import ua.od.atomspace.model.Student;
import ua.od.atomspace.service.StudentIOService;
import ua.od.atomspace.service.StudentIOServiceImpl;
import ua.od.atomspace.service.StudentService;
import ua.od.atomspace.service.StudentServiceImpl;
import ua.od.atomspace.service.ValidatorService;
import ua.od.atomspace.service.ValidatorServiceImpl;

import java.util.ArrayList;
import java.util.List;

public class StudentApplicationTest {

  private static final ValidatorService validatorService = new ValidatorServiceImpl();
  private static final StudentService studentService = new StudentServiceImpl();
  private static final StudentIOService studentIoService = new StudentIOServiceImpl(validatorService);

  @Test
  public void outputTest() {
    System.out.println("========================");
    System.out.println();
    System.out.println("Output test");
    System.out.println();
    Student student = new Student("Kalina A.V.", 255555l, (short) 2020);
    studentIoService.output(student);
    System.out.println("========================");

  }

  @Test
  public void printCourseByDateTest() {
    System.out.println("========================");
    System.out.println();
    System.out.println("printCourseByDateTest test");
    System.out.println();
    Student student = new Student("Kalina A.V.", 255555l, (short) 2020);
    String date = "20/10/2020 00:00";
    studentService
        .printCourseByDate(date, student);
    System.out.println("========================");
  }

  /**
   * Need to change System.in to custom
   */
//  @Test
//  public void inputStudentTest() {
//    Student student = new Student();
//    studentIoService.input(student);
//  }

  @Test
  public void filterStudentsWithYearOfAdmissionTest() {
    System.out.println("========================");
    System.out.println();
    System.out.println("filterStudentsWithYearOfAdmissionTest test");
    System.out.println();

    List<Student> students = new ArrayList<>();
    students.add(new Student("Kalina A.V.", 255555l, (short) 2020));
    students.add(new Student("Aalinaaa B.V.", 2555523l, (short) 2018));
    students.add(new Student("Aalinaaa B.V.", 2555523l, (short) 2018));
    students.add(new Student("Aalinaaa B.V.", 2555523l, (short) 2018));
    students.add(new Student("Kalna V.Z.", 255524234l, (short) 2019));
    students.add(new Student("Kalllina M.V.", 25552255l, (short) 2020));
    students.add(new Student("Alina V.I.", 255566655l, (short) 2020));
    students.add(new Student("Alinaaaa U.V.", 255557775l, (short) 2050));

    studentService.filterStudentsWithYearOfAdmission((short) 2020, students).forEach(System.out::println);
    System.out.println("========================");
  }
  @Test
  public void printCountStudentsWhereLastNameStartsWithATest() {
    System.out.println("========================");
    System.out.println();
    System.out.println("printCountStudentsWhereLastNameStartsWithATest test");
    System.out.println();

    List<Student> students = new ArrayList<>();
    students.add(new Student("Kalina A.V.", 255555l, (short) 2020));
    students.add(new Student("Aalinaaa B.V.", 2555523l, (short) 2018));
    students.add(new Student("Aalinaaa B.V.", 2555523l, (short) 2018));
    students.add(new Student("Aalinaaa B.V.", 2555523l, (short) 2018));
    students.add(new Student("Kalna V.Z.", 255524234l, (short) 2019));
    students.add(new Student("Kalllina M.V.", 25552255l, (short) 2020));
    students.add(new Student("Alina V.I.", 255566655l, (short) 2020));
    students.add(new Student("Alinaaaa U.V.", 255557775l, (short) 2050));

    studentService.printCountStudentsWhereLastNameStartsWithA(students);
    System.out.println("========================");
  }
}
