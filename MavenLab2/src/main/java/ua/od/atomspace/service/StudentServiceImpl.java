package ua.od.atomspace.service;

import ua.od.atomspace.model.Student;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.List;
import java.util.stream.Collectors;

public class StudentServiceImpl implements StudentService {

  private final static String DATE_PATTERN = "dd/MM/yyyy HH:mm";

  private final static Integer MONTH_OF_START = 9;

  private final static Integer DAY_OF_START = 9;

  @Override
  public void printCourseByDate(String date, Student student) throws RuntimeException {
    DateTimeFormatter dateTimeFormatter = new DateTimeFormatterBuilder()
        .appendPattern(DATE_PATTERN)
        .parseCaseInsensitive()
        .toFormatter();

    LocalDateTime localDateTimeToFind = LocalDateTime.parse(date, dateTimeFormatter);

    LocalDate date1 = LocalDate.of(student.getYearOfAdmission(), MONTH_OF_START, DAY_OF_START);
    LocalDateTime startOfStudy = LocalDateTime.of(date1, LocalTime.MIN);

    boolean YEAR_MORE_ON_ONE = (localDateTimeToFind.getYear() - startOfStudy.getYear()) == 1;
    boolean YEAR_MORE_ON_TWO = (localDateTimeToFind.getYear() - startOfStudy.getYear()) == 2;
    boolean YEAR_MORE_ON_THREE = (localDateTimeToFind.getYear() - startOfStudy.getYear()) == 3;
    boolean YEAR_MORE_ON_FOUR = (localDateTimeToFind.getYear() - startOfStudy.getYear()) == 3;

    boolean MONTH_IN_SECOND_PART = localDateTimeToFind.getMonth().getValue() <= 6;

    byte courseResult;
    if (localDateTimeToFind.getYear() == startOfStudy.getYear()) {
      courseResult = 1;
    } else if (YEAR_MORE_ON_ONE || (YEAR_MORE_ON_TWO && MONTH_IN_SECOND_PART)) {
      courseResult = 2;
    } else if (YEAR_MORE_ON_TWO || (YEAR_MORE_ON_THREE && MONTH_IN_SECOND_PART)) {
      courseResult = 3;
    } else if (YEAR_MORE_ON_THREE || YEAR_MORE_ON_FOUR) {
      courseResult = 4;
    } else {
      System.out.println("The student is not studying during this period ");
      return;
    }
    System.out.println(courseResult + "th Course");
  }

  @Override
  public List<Student> filterStudentsWithYearOfAdmission(short year, List<Student> studentList) {
    return studentList.stream()
        .filter(student -> student.getYearOfAdmission() == year)
        .collect(Collectors.toList());
  }

  @Override
  public void printCountStudentsWhereLastNameStartsWithA(List<Student> studentList) {
    System.out.println("Count: " +
        studentList.stream()
            .filter(student -> student.getLastNameWithInitials().charAt(0) == 'A')
            .count()
    );
  }
}
