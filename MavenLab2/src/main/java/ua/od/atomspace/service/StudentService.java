package ua.od.atomspace.service;

import ua.od.atomspace.model.Student;

import java.util.List;

public interface StudentService {
  void printCourseByDate(String date, Student student) throws RuntimeException;

  List<Student> filterStudentsWithYearOfAdmission(short year, List<Student> studentList);

  void printCountStudentsWhereLastNameStartsWithA(List<Student> studentList);
}
