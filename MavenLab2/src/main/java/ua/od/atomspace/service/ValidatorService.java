package ua.od.atomspace.service;

public interface ValidatorService {
  boolean validateLastNameWithInitials(String lastNameWithInitials);

  boolean validateRecordBookNumber(Long recordBookNumber);

  boolean validateYearOfAdmission(Short yearOfAdmission);

}
