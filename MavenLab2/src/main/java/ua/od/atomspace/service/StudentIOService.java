package ua.od.atomspace.service;

import ua.od.atomspace.model.Student;

public interface StudentIOService {
  void input(Student student);

  void output(Student student);
}
