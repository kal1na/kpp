package ua.od.atomspace.service;

import ua.od.atomspace.model.Student;

import java.util.Scanner;

public class StudentIOServiceImpl implements StudentIOService {

  private final ValidatorService validatorService;
  private final Scanner scanner = new Scanner(System.in);

  public StudentIOServiceImpl(ValidatorService validatorService) {
    this.validatorService = validatorService;
  }

  @Override
  public void input(Student student) {
    inputLastNameWithInitials(student);
    inputRecordBookNumber(student);
    inputYearOfAdmission(student);
  }

  private void inputLastNameWithInitials(Student student) {
    System.out.println("Please enter lastname with initials (example: Kalina V.V.): ");
    try {
      String lastNameWithInitials = scanner.nextLine();
      while (!validatorService.validateLastNameWithInitials(lastNameWithInitials)) {
        System.out.println("Try again, validation error");
        lastNameWithInitials = scanner.nextLine();
      }
      student.setLastNameWithInitials(lastNameWithInitials);
    } catch (Exception ex) {
      System.out.println("Wrong type exception \n");
    }

  }

  private void inputRecordBookNumber(Student student) {
    System.out.println("Please enter record book number: ");
    try {
      long recordBookNumber = scanner.nextLong();
      while (!validatorService.validateRecordBookNumber(recordBookNumber)) {
        System.out.println("Try again, validation error");
        recordBookNumber = scanner.nextLong();
      }
      student.setRecordBookNumber(recordBookNumber);
    } catch (Exception ex) {
      System.out.println("Wrong type exception \n");
    }
  }

  private void inputYearOfAdmission(Student student) {
    System.out.println("Please enter year of admission: ");
    try {
      short yearOfAdmission = scanner.nextShort();
      while (!validatorService.validateYearOfAdmission(yearOfAdmission)) {
        System.out.println("Try again, validation error");
        yearOfAdmission = scanner.nextShort();
      }
      student.setYearOfAdmission(yearOfAdmission);
    } catch (Exception ex) {
      System.out.println("Wrong type exception \n");
    }
  }


  @Override
  public void output(Student student) {
    System.out.println(student);
  }
}
