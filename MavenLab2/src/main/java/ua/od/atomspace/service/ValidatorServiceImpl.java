package ua.od.atomspace.service;

public class ValidatorServiceImpl implements ValidatorService {

  private final static Short MINIMUM_YEAR_OF_ADMISSION = 2000;
  private final static String LASTNAME_WITH_INITIALS_REGEXP = "^[A-Z][a-z]{1,19}\\s[A-Z]\\.\\s?[A-Z]\\.";

  @Override
  public boolean validateLastNameWithInitials(String lastNameWithInitials) {
    return lastNameWithInitials.matches(LASTNAME_WITH_INITIALS_REGEXP);
  }

  @Override
  public boolean validateRecordBookNumber(Long recordBookNumber) {
    return recordBookNumber > 0;
  }

  @Override
  public boolean validateYearOfAdmission(Short yearOfAdmission) {
//    return yearOfAdmission >= MINIMUM_YEAR_OF_ADMISSION && yearOfAdmission < LocalDateTime.now().getYear();
    return yearOfAdmission >= MINIMUM_YEAR_OF_ADMISSION;
  }

}
