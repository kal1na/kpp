package ua.od.atomspace.model;

public class Student {
  private String lastNameWithInitials;

  private Long recordBookNumber;

  private Short yearOfAdmission;

  public Student(String lastNameWithInitials, Long recordBookNumber, Short yearOfAdmission) {
    this.lastNameWithInitials = lastNameWithInitials;
    this.recordBookNumber = recordBookNumber;
    this.yearOfAdmission = yearOfAdmission;
  }

  public Student() {
  }

  public String getLastNameWithInitials() {
    return lastNameWithInitials;
  }

  public void setLastNameWithInitials(String lastNameWithInitials) {
    this.lastNameWithInitials = lastNameWithInitials;
  }

  public Long getRecordBookNumber() {
    return recordBookNumber;
  }

  public void setRecordBookNumber(Long recordBookNumber) {
    this.recordBookNumber = recordBookNumber;
  }

  public Short getYearOfAdmission() {
    return yearOfAdmission;
  }

  public void setYearOfAdmission(Short yearOfAdmission) {
    this.yearOfAdmission = yearOfAdmission;
  }

  @Override
  public String toString() {
    return "ua.od.atomspace.model.Student{" +
        "lastNameWithInitials='" + lastNameWithInitials + '\'' +
        ", recordBookNumber=" + recordBookNumber +
        ", yearOfAdmission=" + yearOfAdmission +
        '}';
  }
}
