package ua.od.atomspace;

import ua.od.atomspace.model.Student;
import ua.od.atomspace.service.StudentIOService;
import ua.od.atomspace.service.StudentIOServiceImpl;
import ua.od.atomspace.service.ValidatorService;
import ua.od.atomspace.service.ValidatorServiceImpl;

public class StudentApplication {

  private static final ValidatorService validatorService = new ValidatorServiceImpl();
  private static final StudentIOService studentIoService = new StudentIOServiceImpl(validatorService);

  public static void main(String[] args) {
    Student student = new Student();
    System.out.println("Input student info \n========================== \n");
    studentIoService.input(student);
  }
}
